/** \brief   Array wit the layout of the flash memory.
 *  \details Also controls what part of the flash memory is reserved for the bootloader.
 *           If the bootloader size changes, the reserved sectors for the bootloader
 *           might need adjustment to make sure the bootloader doesn't get overwritten.
 *           The current flash layout does not reflect the minimum sector size of the
 *           physical flash (1 - 2kb), because this would make the table quit long and
 *           a waste of ROM. The minimum sector size is only really needed when erasing
 *           the flash. This can still be done in combination with macro
 *           FLASH_ERASE_BLOCK_SIZE.
 */
static const tFlashSector flashLayout[] =
{
  /* space is reserved for a bootloader configuration with all supported communication
   * interfaces enabled. when for example only UART is needed, than the space required
   * for the bootloader can be made a lot smaller here.
   */
  /* { 0x08000000, 0x02000 },           flash sector  0 - reserved for bootloader   */
  { 0x08002000, 0x02000 },
  { 0x08004000, 0x02000 },
  { 0x08006000, 0x02000 },           /* flash sector  3 - 8kb                       */
#if (BOOT_NVM_SIZE_KB > 32)
  { 0x08008000, 0x02000 },           /* flash sector  4 - 8kb                       */
  { 0x0800A000, 0x02000 },           /* flash sector  5 - 8kb                       */
  { 0x0800C000, 0x02000 },           /* flash sector  6 - 8kb                       */
  { 0x0800E000, 0x02000 },           /* flash sector  7 - 8kb                       */
#endif
#if (BOOT_NVM_SIZE_KB > 64)
  { 0x08010000, 0x02000 },           /* flash sector  8 - 8kb                       */
  { 0x08012000, 0x02000 },           /* flash sector  9 - 8kb                       */
  { 0x08014000, 0x02000 },           /* flash sector 10 - 8kb                       */
  { 0x08016000, 0x02000 },           /* flash sector 11 - 8kb                       */
  { 0x08018000, 0x02000 },           /* flash sector 12 - 8kb                       */
  { 0x0801A000, 0x02000 },           /* flash sector 13 - 8kb                       */
  { 0x0801C000, 0x02000 },           /* flash sector 14 - 8kb                       */
  { 0x0801E000, 0x02000 },           /* flash sector 15 - 8kb                       */
#endif
#if (BOOT_NVM_SIZE_KB > 128)
  { 0x08020000, 0x08000 },           /* flash sector 16 - 32kb                      */
  { 0x08028000, 0x08000 },           /* flash sector 17 - 32kb                      */
  { 0x08030000, 0x08000 },           /* flash sector 18 - 32kb                      */
  { 0x08038000, 0x08000 },           /* flash sector 19 - 32kb                      */
#endif
#if (BOOT_NVM_SIZE_KB > 256)
  { 0x08040000, 0x08000 },           /* flash sector 20 - 32kb                      */
  { 0x08048000, 0x08000 },           /* flash sector 21 - 32kb                      */
  { 0x08050000, 0x08000 },           /* flash sector 22 - 32kb                      */
  { 0x08058000, 0x08000 },           /* flash sector 23 - 32kb                      */
  { 0x08060000, 0x08000 },           /* flash sector 24 - 32kb                      */
  { 0x08068000, 0x08000 },           /* flash sector 25 - 32kb                      */
  { 0x08070000, 0x08000 },           /* flash sector 26 - 32kb                      */
  { 0x08078000, 0x08000 },           /* flash sector 27 - 32kb                      */
#endif
#if (BOOT_NVM_SIZE_KB > 512)
#error "BOOT_NVM_SIZE_KB > 512 is currently not supported."
#endif
};

